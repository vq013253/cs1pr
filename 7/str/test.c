#include <stdio.h>
#include <stdlib.h>
#include <cstrlib.h>
#include <string.h>
void operationManager(char* op, char* inp){

  if(strcmp(op, "L") == 0){
    lengthofString(inp);
 } else if(strcmp(op, "B") == 0){
   bytesofString(inp);
 } else if(strcmp(op, "H") == 0){
   hashofString("Invalid input\n");
  } else{
   printf("Invalid input\n");
  }
}
int main(int argc, char ** argv) {
char* operation;
char* input;
if(argc >=2){
operation = argv[1];
input = argv[2];

 operationManager(operation, input);
 }else{
 printf("Not enough arguments\n");
 }
}
