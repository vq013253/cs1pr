#include <stdio.h>
  int main(){
  float r = 4.5;
  float circ = r * r * 3.141597;

  printf("The circumference of the circle with radius %f is %f", r, circ);

  return 0;
}
