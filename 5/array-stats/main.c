#include <stdio.h>
 #include <math.h>
 #include "my_lib.h"

 double mean(int arr[], int n) {
   int count = n;
   int sum = 0;
   double avarage;

   for(int i = 0; i < n; i++) {
     sum += arr[i];
   }

   avarage = (double) sum / count;
   return avarage;

 }

  int max(int arr[],int n) {
    int max = 0;

     for(int  i = 0; i < n; i++) {

       if(max < arr[i]) {
         max = arr[i];
       }

     }

      return max;
   }

 double sd(int arr[], int n) {
   double av = mean(arr, n);
   double newArr[n];
   double deviation;
   double variance;
   double sum = 0;
   int count = n;

 for (int  i = 0; i < n; i++) {
   newArr[i] = (arr[i] - av) * (arr[i] - av);
 }
  for(int  i = 0; i < n; i++) {
    sum += newArr[i];
  }

  variance = (double) sum/count;
  deviation = sqrt(variance);

  return deviation;
 }
