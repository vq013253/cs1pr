#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "my_lib.h"

void inputManager(char * inp, int arr[], int n) {

  if (strcmp(inp, "mean") == 0 ) {

    printf("%.1f\n", mean(arr,n));
  }
  else if (strcmp(inp, "max") == 0 ) {
     printf("%.1f\n", (float)max(arr, n));
  } else if (strcmp(inp,"sd") == 0 ) {
     printf("%.1f\n", sd(arr, n));
  } else {
    printf("invalid input\n");
  }

}

  int main(int argc, char ** argv) {
    int i = 0;
    int arr[argc - 2];
    char * operation = argv[1];

    for(int i=0; i< argc-2; i++)
    {
      arr[i] = atoi(argv[i+2]);
    }

    inputManager(operation, arr, argc-2);
  }
