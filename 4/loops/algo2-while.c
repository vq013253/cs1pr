#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int main(int argc, char ** argv[])
{
char * str = argv[1];
int n = strlen(str);
int i=0;
bool palindrome = true;
while(i<n/2){
i++;
if (str[i] != str[n-i-1])
{
palindrome = false;
break;
}
}
if(palindrome){
printf("palindrome\n");
}
else
{
printf("no palindrome\n");
}
return 0;
}
