#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int addNumbers(int n);

int main(int argc, char ** argv[]){
int num = atoi(argv[1]);
float mean;

mean = (float)addNumbers(num)/num;
printf("%d %.1f\n",addNumbers(num),mean);
return 0;
      }

int addNumbers(int n){
if(n != 0)
return n + addNumbers(n - 1);
else{
return n;
}
}
